package Tales;

public class Titulos {

    public Titulos() {
    }

    public static void inicio() {
        System.out.println("\n" +
                " ___                                                             ___                     __         __             \n" +
                "-   ---___-       ,,                           /\\         -_-/  -   ---___- -__ /\\     ,-||-,      /  -, _-_-,     \n" +
                "   (' ||      _   ||                          ||         (_ /      (' ||      || \\,   ('|||  )    ||   )   // ,    \n" +
                "  ((  ||     < \\, ||  _-_   _-_,        /'\\\\ =||=       (_ --_    ((  ||     /|| /   (( |||--))  ~||---)   ||/\\\\   \n" +
                " ((   ||     /-|| || || \\\\ ||_.        || ||  ||          --_ )  ((   ||     \\||/-   (( |||--))  ~||---,  ~|| <    \n" +
                "  (( //     (( || || ||/    ~ ||       || ||  ||         _/  ))   (( //       ||  \\   ( / |  )   ~||  /    ||/\\\\   \n" +
                "    -____-   \\/\\\\ \\\\ \\\\,/  ,-_-        \\\\,/   \\\\,       (_-_-       -____-  _---_-|,   -____-     |, /    _-__,\\\\, \n" +
                "                                                                                                -_-  --~           \n");
    }

    public static void finalJuego() {
        System.out.println("Has llegado al catillo oscuro. Espero que tu camino haya sido agradable y hayas disfrutado.\n El juego aún no esta termiado y con el tiempo se le añadira nuevo contenido. Mantente atento");
        System.out.println("Espero que hayas disfrutado de Tales of Stronk. Recuerda que Tales of Stronk sigue en desarrollo y llegaran nuevas expansiones del juego.");
    }

    public static void creditos() throws InterruptedException{
        System.out.println("Tales of Stronk.");

        Thread.sleep(1000);

        System.out.println("Desarrollado por DrunkPsyduck (Mario)");
        Thread.sleep(1000);

        System.out.println("Idea original: Drunk Psyduck");
        Thread.sleep(1000);

        System.out.println("Jefe diseño: Drunk Psyduck");
        Thread.sleep(1000);

        System.out.println("Jefe equipo: Drunk Psyduck");
        Thread.sleep(1000);

        System.out.println("Director: Drunk Psyduck");
        Thread.sleep(1000);

        System.out.println("Madrid 2019");
        Thread.sleep(1000);

        System.out.println("Versión 0.1");//Aumentar con el numero de versiones
        Thread.sleep(1000);

        System.out.println("Gracias a aquellos que lo jugais y a aquellos que me han ayudado durante su desarrollo, sea probandolo o programandolo.");
        Thread.sleep(1000);

        System.out.println("Tales of Stronk sigue en desarrollo. Si te ha gustado sigue el progreso del proyecto en https://github.com/DrunkPsyduck/TalesOfStronk");
    }
}
