package Tales;

public class Mundo {
    private static int coordenadaX;
    private static int coordenadaY;

    public Mundo() {
        coordenadaX = 0;
        coordenadaY=0;
    }

    public Mundo(int coordenadaX, int coordenadaY) {
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
    }

    public static void mapa(){
        System.out.println("estas en las coordenadas " + coordenadaY + "N" + coordenadaX+"E");
    }

    public static void moverNorte(){
        coordenadaY += 1;
        System.out.println("Te mueves al norte");
    }

    public static void moverSur(){
        coordenadaY += -1;
        System.out.println("Te mueves al Sur");
    }

    public static void moverEste(){
        coordenadaX += 1;
        System.out.println("Te mueves al este");
    }

    public static void moverOeste(){
        coordenadaX += -1;
        System.out.println("Te mueves al oeste");
    }

    public static void aldeaOlvidada(){
        if (coordenadaY == 20 && coordenadaX==15){
            System.out.println("Estas en Aldea Olvidada. Parece que hace tiempo que nadie habita aqui. Ahora solo coje polvo");
        }
    }

    public static boolean castilloOscuro(){
        if(coordenadaY==180&&coordenadaX==-45){
            return true;
        } else {
            return false;
        }
    }

    //Metodo Acceso rapido castillo oscuro. Borrar Al terminar

    public static void accesoRapido(){
        coordenadaY=180;
        coordenadaX = -45;
    }

    @Override
    public String toString() {
        return "Estas en las coordenadas " + coordenadaX + "," + coordenadaY;
    }
}
