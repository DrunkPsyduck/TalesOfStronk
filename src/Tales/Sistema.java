package Tales;

public class Sistema {

    public Sistema() {
    }

    public static void clearScreen() {
        System.out.println("\033[H\033[2J");
        System.out.flush();
    }

    public final static void clearConsole(){
        /* El metodo funciona cuando se ejecuta desde consola. Por lo tanto al ejecutarlo en el IDE no mostrar ni realizara nada */
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                Runtime.getRuntime().exec("cls");
            }
            else
            {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e)
        {
            //  Handle any exceptions.
            e.getMessage();
        }
    }
}
