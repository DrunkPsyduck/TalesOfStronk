package Tales;

public class Espada extends Items {
    private int daño;

    public Espada(int peso, String estado, int daño) {
        super(peso, estado);
        this.daño = daño;
    }

    public int getDaño() {
        return daño;
    }

    public void setDaño(int daño) {
        this.daño = daño;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + super.toString() + " daño " + daño;
    }
}
