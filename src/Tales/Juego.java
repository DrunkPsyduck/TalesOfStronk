package Tales;

import java.util.ArrayList;



public class Juego {
    /* Esta clase se encarga de la posicion del jugador a lo largo de su aventura */


    protected static ArrayList<Items> mochila;

    public Juego() {
        mochila = new ArrayList<>();
    }

    public void nuevoJuego(Jugador jugador) throws InterruptedException { /* Este metodo se encarga de crear la partida*/
        System.out.println("Bienvenido a Tales of Stronk. Un juego de aventuras que te transladara al lejano mundo de Stronk\n En Stronk todo es diferente y extraño. ten cuidado en tu camino");

        jugador.nuevoNombreJugador();

        Thread.sleep(1500);

        Sistema.clearConsole();//limpiar pantalla

        inicio();//muestra un mensaje de inicio

        while (Mundo.castilloOscuro() == false) {

            camino();//camino() pide un punto cardina por el que deseas ir. norte sur este y oeste

            //batallas

            if (Batallas.batallaAleatorea()){
                Batallas.batallas();
            }

            if (cofre()) {
                if (abrirCofre()) {
                    cofreAleatoreoComun();
                }
            }

            //Generacion de lugares
            Mundo.aldeaOlvidada();


        }
        Titulos.finalJuego();

    }

    public void inicio() {
        System.out.println();
        System.out.println("Tu misión es llegar al castillo oscuro. Nadie sabe donde esta. Algunos dicen que cambia de lugar. Por suerte tu tienes un mapa que te dice en que posicion estas");
        System.out.println("Si quieres saber tu posicion solo tienes que escribir 'mapa'");
    }

    public void camino() {
        System.out.println("Que haces?(norte/sur/este/oeste/mapa/mochila)");
        String opciones = EntradaSalida.leerTexto();

        boolean valido = false;

        while (!valido) {
            valido = true;
            if (opciones.equalsIgnoreCase("norte")) {
                Mundo.moverNorte();
            } else if (opciones.equalsIgnoreCase("sur")) {
                Mundo.moverSur();
            } else if (opciones.equalsIgnoreCase("este")) {
                Mundo.moverEste();
            } else if (opciones.equalsIgnoreCase("oeste")) {
                Mundo.moverOeste();
            } else if (opciones.equalsIgnoreCase("mapa")) {
                Mundo.mapa();
            } else if (opciones.equalsIgnoreCase("mochila")) {
                mostrarMochila();
            } else if (opciones.equalsIgnoreCase("castillo")) {
               /*Metodo para comprobar funcionamiento sin llegar al final
                    Borrar al terminar
                */
                Mundo.accesoRapido();

            } else {
                System.out.println("introduce una direccion valida");
            }
        }

    }

    public boolean cofre() {
        int rnd = (int) (Math.random() * 50);

        if (rnd < 5) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean abrirCofre() {
        System.out.println("Has encontrado un cofre.¿Deseas abrirlo?(si/no)");

        String abrir = EntradaSalida.leerTexto();

        if (abrir.equalsIgnoreCase("si")) {
            return true;
        } else {
            return false;
        }
    }

    public void cofreAleatoreoComun() {
        /* generara un cofre que contendra un elemento */
        int nAleatoreo = (int) (Math.random() * 100);

        //Metodo apertura cofre
        if (abrirCofre()) {
            System.out.print("En el cofre has encontrado ");
            itemAleatoreo();
            System.out.println();
        }

    }


    public void itemAleatoreo() {
        int itemAleatoreo = (int) (Math.random() * 30);

        if (itemAleatoreo > 0 && itemAleatoreo < 10) {
            System.out.println("espada");
            mochila.add(new Espada(10, "nuevo", itemAleatoreo));
        } else if (itemAleatoreo > 10 && itemAleatoreo < 20) {
            System.out.println("hacha");
            mochila.add(new Hacha(5, "nuevo", itemAleatoreo));
        } else {
            //solo una pistola. Si ya hay pistola se añadira muncion
            System.out.println("pistola/munición");
            boolean encontrado = false;
            for (int i = 0; i < mochila.size() && !encontrado; i++) {
                if (mochila.get(i) instanceof Pistola) {
                    mochila.add(new Municion(10));
                } else {
                    mochila.add(new Pistola(5, "nuevo", 10));
                    encontrado = true;
                }
            }
        }
    }

    //mochila. Muestra los objetos disponibles

    public void mostrarMochila() {

        if (mochila.size() == 0) {
            System.out.println("No tienes nada en la mochila");
        } else {
            for (int i = 0; i < mochila.size(); i++) {
                System.out.println(mochila.get(i));
            }
        }
    }
}
