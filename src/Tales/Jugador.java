package Tales;

public class Jugador {
    /*Colores*/
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    private String nombreJugador;
    private int vidas;

    public Jugador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
        this.vidas = 1;
    }

    public Jugador() {

    }

    public void nuevoNombreJugador(){
        System.out.println("Bienvenido forastero.¿Cúal es tu nombre?");
        String nombre = EntradaSalida.leerTexto();

        System.out.println(nombre +" estas a punto de comenzar tu aventura. Buena suerte.");

        setNombreJugador(nombre);
    }

    public String getNombreJugador() {
        return nombreJugador;
    }

    public void setNombreJugador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

    @Override
    public String toString() {
        return nombreJugador + " tienes " + vidas;
    }
}
