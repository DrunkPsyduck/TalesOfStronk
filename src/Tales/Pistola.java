package Tales;

public class Pistola extends Items {
    /* item que se encontrara en los cofres.
    si no se tiene muncion no se puede disparar
     */

    private int municionDisponible;
    private static int daño;

    public Pistola(int peso, String estado) {
        super(peso, estado);
        municionDisponible = 10;
        daño = 105;
    }

    public Pistola(int peso, String estado, int municionDisponible) {
        super(peso, estado);
        this.municionDisponible = municionDisponible;
    }

    public int getMunicionDisponible() {
        return municionDisponible;
    }

    public void setMunicionDisponible(int municionDisponible) {
        this.municionDisponible = municionDisponible;
    }


    public static int getDaño() {
        return daño;
    }

    public static void setDaño(int daño) {
        Pistola.daño = daño;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + super.toString() + " munición  " + municionDisponible ;
    }
}
