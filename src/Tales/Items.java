package Tales;

public abstract class Items {
    private int peso;
    private String estado;

    public Items() {

    }

    public Items(int peso, String estado) {
        this.peso = peso;
        this.estado = estado;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return " Peso " + peso +
                " estado " + estado;
    }
}
