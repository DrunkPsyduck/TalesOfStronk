package Tales;

public class Municion extends Items{
    /* municion para la pistola*/

    private int balas;

    public Municion() {
        balas = 0;
    }

    public Municion(int balas) {
        this.balas += balas;
    }

    public int getBalas() {
        return balas;
    }

    public void setBalas(int balas) {
        this.balas = balas;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "Munición restante " + balas;
    }
}
