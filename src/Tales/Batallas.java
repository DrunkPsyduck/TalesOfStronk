package Tales;

import javax.swing.*;
import java.util.ArrayList;

public class Batallas {

    //todo Terminar menu batalla e integrarlo con el juegp
    private static ArrayList<Items> aux;

    public Batallas() {
    }

    public static  boolean batallaAleatorea(){
        int rnd =(int)(Math.random()*50);
        if (rnd < 5){
            return true;
        } else {
            return false;
        }
    }
    public static void batallas(){
        aux = Juego.mochila;

        ArrayList<Enemigos> enemigos = new ArrayList<>();

        int nEnemigos = (int)(Math.random()+5);

        for (int i = 0; i < nEnemigos; i++) {
            enemigos.add(new Enemigos());
        }

        boolean huyes = false;

        while(enemigos.size()>0 && !huyes){
            System.out.println("Han aparecido enemigos. Hora de luchar");

            System.out.println("Atacas o huyes");
            String opcionCombate = EntradaSalida.leerTexto();

            if (opcionCombate.equalsIgnoreCase("atacar")){
                if (Juego.mochila.size()==0){
                    System.out.println("No tienes nada con lo que atacar");
                } else {
                    boolean muerto = false;
                    for (int i = 0; i < Juego.mochila.size() && !muerto; i++) {
                        if (Juego.mochila.get(i) instanceof Espada){
                            System.out.println("atacas con tu espada");
                            Enemigos.setPuntosVida(((Espada) Juego.mochila.get(i)).getDaño() - Enemigos.getPuntosVida());
                            if (((Espada) Juego.mochila.get(i)).getDaño() - Enemigos.getPuntosVida() <=0){
                                muerto = true;
                            }
                        } else if (Juego.mochila.get(i) instanceof Hacha){
                            System.out.println("atacas con tu Hacha");
                            Enemigos.setPuntosVida(((Hacha) Juego.mochila.get(i)).getDaño() - Enemigos.getPuntosVida());
                            if (((Hacha) Juego.mochila.get(i)).getDaño() - Enemigos.getPuntosVida() <=0){
                                muerto = true;
                            }
                        } else if (Juego.mochila.get(i) instanceof Pistola){
                            System.out.println("atacas con tu espada");
                            Enemigos.setPuntosVida(((Pistola) Juego.mochila.get(i)).getDaño() - Enemigos.getPuntosVida());
                            if (((Pistola) Juego.mochila.get(i)).getDaño() - Enemigos.getPuntosVida() <=0){
                                muerto = true;
                            }
                        }
                    }
                }
            } else {
                System.out.println("intentas huir");
                int escape = (int)(Math.random()+6);

                if (escape%2==0){
                    for (int i = 0; i < enemigos.size(); i++) {
                       huyes = true;
                    }
                    System.out.println("Has escapado");
                } else {
                    System.out.println("Intentas escapar pero no puedes");
                }


            }

        }
    }

}
