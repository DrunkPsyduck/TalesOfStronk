package Tales;

public class Enemigos {

    private static int puntosVida;

    public Enemigos(int vidas) {
        this.puntosVida = vidas;
    }

    public Enemigos(){
        puntosVida = 50;
    }

    public static void minion(){
        puntosVida=60;
    }

    public static int getPuntosVida() {
        return puntosVida;
    }

    public static void setPuntosVida(int puntosVida) {
        Enemigos.puntosVida = puntosVida;
    }
}