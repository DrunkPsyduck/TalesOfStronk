package Tales;

public class Tester {
    /**
     * Tales Of Stronk
     * @author mario
     * @version 0.1
     * @since 2019
     */
    public static void main(String[] args) {

        Juego j = new Juego();
        Jugador jugador = new Jugador();
        //System.out.println("Tales of STRONk");
        Titulos.inicio();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("Desea iniciar una partida?(Si/no)");//Cuando se pueda cargar desde fichero se añadira la opcion de cargar una nueva partida
        String nuevaPartida = EntradaSalida.leerTexto();

        if (nuevaPartida.equalsIgnoreCase("si")){

            try {
                j.nuevoJuego(jugador);
            } catch (InterruptedException e) {
                System.out.println("No se ha podido iniciar o completar el juego. InterruptedException");
            }
        }
        try {
            Titulos.creditos();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
