package Tales;

public class Hacha extends Items {
    private static int daño;

    public Hacha(int peso, String estado, int daño) {
        super(peso, estado);
        this.daño = daño;
    }

    public static int getDaño() {
        return daño;
    }

    public static void setDaño(int daño) {
        Hacha.daño = daño;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + super.toString() + " daño " + daño;
    }
}
